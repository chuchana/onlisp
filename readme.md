This is a LuaTeX'd copy of Paul Graham's On Lisp with syntax highlighting, derived from the included texinfo copy.
The fonts used are  Linux Libertine O and Inconsolatazi4. 

The missing figures are taken from http://www.lurklurk.org/onlisp/onlisp.html

The master file is onlisp.tex.

I've added some footnotes which mention libraries implementing a production-ready version of the code being discussed, as well as notes about updates to the CL standard. These are introduced with the (TeX) macro \mynote. 


I build this with `latexmk -pdf onlisp.tex` and tell (in .latexmkrc) `latexmk' to use 
```
$pdflatex = 'lualatex -shell-escape -interaction=nonstopmode';
```
for pdflatex


The toggle `mint` turns on syntax highlighting, but this slows down builds and makes noisy output, so it is easier to debug if this toggle is false. You need the python package [Pygments](http://pygments.org/download/) for syntax highlighting. You can install it with `pip install pygments`.

To just check the syntax, you can use `\syntaxonly` (or uncomment the appropriate line).

And, of course, if you are only working on one part, you can use `\includeonly{part#}`

Happy Hacking! 
